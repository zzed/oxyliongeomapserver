<a name="Mapa"></a>

## Mapa
Class representing the map

**Kind**: global class  

* [Mapa](#Mapa)
    * [new Mapa(output)](#new_Mapa_new)
    * [.averageGeolocation(coords)](#Mapa+averageGeolocation) ⇒ <code>number</code>
    * [.drawTransceiver(payload, icon)](#Mapa+drawTransceiver)
    * [.drawClient(payload, icon)](#Mapa+drawClient)
    * [.drawEveryone()](#Mapa+drawEveryone)
    * [.setView(coords, coords[)](#Mapa+setView)
    * [.prepareMap(coords)](#Mapa+prepareMap)

<a name="new_Mapa_new"></a>

### new Mapa(output)
Create the map


| Param | Type | Description |
| --- | --- | --- |
| output | <code>string</code> | DOM div id where the map will render |

<a name="Mapa+averageGeolocation"></a>

### mapa.averageGeolocation(coords) ⇒ <code>number</code>
Calculate the average of points in an array

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  
**Returns**: <code>number</code> - Center between every point  

| Param | Type | Description |
| --- | --- | --- |
| coords | <code>Array.&lt;Object&gt;</code> | Coordinates of every point |
| coords[].latitude | <code>number</code> | Latitude of a point |
| coords[].longitude | <code>number</code> | Longitude of a point |

<a name="Mapa+drawTransceiver"></a>

### mapa.drawTransceiver(payload, icon)
Draw a transceiver

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Object</code> | Payload |
| payload.color | <code>string</code> | Color of the border of the transceiver range |
| payload.fillColor | <code>string</code> | Color of the filling of the transceiver range |
| payload.fillOpacity | <code>number</code> | Opacity of the filling |
| payload.radius | <code>number</code> | Radius of the range |
| payload.formattedAddress | <code>string</code> | Address of the transceiver |
| payload.coords[ | <code>Array.&lt;number&gt;</code> | Coordinates of the transceiver (latitude, longitude) |
| icon | <code>Object</code> | Leaflet icon for the transceiver |

<a name="Mapa+drawClient"></a>

### mapa.drawClient(payload, icon)
Draw a client

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Object</code> | Payload |
| payload.coords | <code>Array.&lt;number&gt;</code> | Coordinates of the client (latitude, longitude) |
| payload.formattedAddress | <code>string</code> | Client's address |
| icon | <code>Object</code> | Leaflet icon for the client |

<a name="Mapa+drawEveryone"></a>

### mapa.drawEveryone()
Draw everyone from the clients object

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  
<a name="Mapa+setView"></a>

### mapa.setView(coords, coords[)
Set the view of the map

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  

| Param | Type | Description |
| --- | --- | --- |
| coords | <code>Array.&lt;Object&gt;</code> | Array of places |
| coords[ | <code>Object</code> | A single place |
| coords[].latitude | <code>number</code> | Latitude of the place |
| coords[].longitude | <code>number</code> | Longitude of the place |
| coords[].formattedAddress | <code>string</code> | Address of the place |

<a name="Mapa+prepareMap"></a>

### mapa.prepareMap(coords)
Prepare the map: set the viewport of the map, move the clients to an internal array, draw everyone

**Kind**: instance method of [<code>Mapa</code>](#Mapa)  

| Param | Type | Description |
| --- | --- | --- |
| coords | <code>Array.&lt;Object&gt;</code> | Array of places |

