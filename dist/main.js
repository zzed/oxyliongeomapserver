/**
 * Class representing the map
 */

class Mapa {
  /**
   * Create the map
   * @param {string} output DOM div id where the map will render
   */
  constructor(output) {
    this.output = output // Where does the map output? (id name)
    this.clients = [] // Who to draw?
    this.map = L.map(this.output) // Where does the map output? (DOM object)
    this.colors = ['red', 'green', 'yellow'] // Border colors
    this.fillColors = ['#ff0033', '#00ff33', '#ffff33'] // Fill colors
    this.iconsClients = [ // Icons for clients
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/waypoint/pin/red.png',
        iconSize: [24, 24],
        iconAnchor: [0, 24]
      }),
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/waypoint/pin/green.png',
        iconSize: [24, 24],
        iconAnchor: [0, 24]
      }),
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/waypoint/pin/yellow.png',
        iconSize: [24, 24],
        iconAnchor: [0, 24]
      })
    ]
    this.iconsTransceivers = [ // Icons for the transceivers
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/misc/landmark/power/tower.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32]
      }),
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/misc/landmark/lighthouse.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32]
      }),
      L.icon({
        iconUrl: 'https://svn.openstreetmap.org/applications/share/map-icons/classic.big/misc/landmark/crane.png',
        iconSize: [32, 32],
        iconAnchor: [16, 32]
      })
    ]
  }

  /**
   * Calculate the average of points in an array
   * @param {Object[]} coords Coordinates of every point
   * @param {number} coords[].latitude Latitude of a point
   * @param {number} coords[].longitude Longitude of a point
   * @returns {number} Center between every point
   */

  averageGeolocation(coords) {
    if (coords.length === 1) {
      return coords[0]
    }
    let x = 0.0
    let y = 0.0
    let z = 0.0
    for (let coord of coords) {
      let latitude = coord.latitude * Math.PI / 180
      let longitude = coord.longitude * Math.PI / 180
      x += Math.cos(latitude) * Math.cos(longitude)
      y += Math.cos(latitude) * Math.sin(longitude)
      z += Math.sin(latitude)
    }
    let total = coords.length
    x = x / total
    y = y / total
    z = z / total
    let centralLongitude = Math.atan2(y, x)
    let centralSquareRoot = Math.sqrt(x * x + y * y)
    let centralLatitude = Math.atan2(z, centralSquareRoot)
    return [centralLatitude * 180 / Math.PI, centralLongitude * 180 / Math.PI]
  }

  /**
   * Draw a transceiver
   * @param {Object} payload Payload
   * @param {string} payload.color Color of the border of the transceiver range
   * @param {string} payload.fillColor Color of the filling of the transceiver range
   * @param {number} payload.fillOpacity Opacity of the filling
   * @param {number} payload.radius Radius of the range
   * @param {string} payload.formattedAddress Address of the transceiver
   * @param {number[]} payload.coords[] Coordinates of the transceiver (latitude, longitude)
   * @param {Object} icon Leaflet icon for the transceiver
   */

  drawTransceiver(payload, icon) {
    let startAngle = payload.clients[0].angle
    let stopAngle = payload.clients[payload.clients.length - 1].angle
    let radius = payload.clients[0].distance
    payload.clients.forEach(element => {
      if (element.distance > radius) {
        radius = element.distance
      }
    })
    L.semiCircle(payload.coords, {
      color: payload.color,
      fillColor: payload.fillColor,
      fillOpacity: payload.fillOpacity,
      radius,
      startAngle,
      stopAngle
    })
      .addTo(this.map)
      .bindPopup(`Kąt nadajnika: ${(stopAngle - startAngle).toFixed(2)}`)
    L.marker(payload.coords, {
      icon
    })
      .addTo(this.map)
      .bindPopup('Nadajnik: ' + payload.formattedAddress)
  }

  /**
   * Draw a client
   * @param {Object} payload Payload
   * @param {number[]} payload.coords Coordinates of the client (latitude, longitude)
   * @param {string} payload.formattedAddress Client's address
   * @param {Object} icon Leaflet icon for the client
   */

  drawClient(payload, icon) {
    L.marker(payload.coords, {icon}).addTo(this.map)
      .bindPopup('Klient: ' + payload.formattedAddress)
  }

  /**
   * Draw everyone from the clients object
   */

  drawEveryone() {
    let counter = 0 // Counter for different icons and colors

    this.clients.forEach(element => {
      this.drawTransceiver({ // Draw every transceiver
        coords: [
          element.nodeInfo.nodeAddress.latitude,
          element.nodeInfo.nodeAddress.longitude
        ],
        color: this.colors[counter],
        fillColor: this.fillColors[counter % this.iconsClients.length],
        fillOpacity: 0.5,
        formattedAddress: element.nodeInfo.nodeAddress.formattedAddress,
        clients: element.clients
      }, this.iconsTransceivers[counter % this.iconsTransceivers.length])
      element.clients.forEach(client => { // Draw every client
        this.drawClient({
          coords: [
            client.latitude,
            client.longitude
          ],
          formattedAddress: client.formattedAddress,
        }, this.iconsClients[counter])
      })
      counter += 1 // Increase the color/icon-choosing counter by 1
    })
  }

  /**
   * Set the view of the map
   * @param {Object[]} coords Array of places
   * @param {Object} coords[] A single place
   * @param {number} coords[].latitude Latitude of the place
   * @param {number} coords[].longitude Longitude of the place
   * @param {string} coords[].formattedAddress Address of the place
   */
  setView(coords) {
    this.coordinates = this.averageGeolocation(coords)
    this.map.setView(this.coordinates, 13)
    L.tileLayer('https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map)
  }
  /**
   * Prepare the map: set the viewport of the map, move the clients to an internal array, draw everyone
   * @param {Object[]} coords Array of places
   */
  prepareMap(coords) {
    console.log(coords)
    let tempCoords = []
    coords.forEach(element => {
      tempCoords.push(element.nodeInfo.nodeAddress)
      element.clients.forEach(client => tempCoords.push(client))
    });
    this.setView(tempCoords)
    this.clients = coords
    this.drawEveryone()
  }
}

var whatToFetch = [
  {
    'nodeInfo': {
      'nodeAddress': {
        'name': 'Roosevelta 22, Poznań, PL'
      }
    },
    'clients': [
      {
        'name': 'Przybyszewskiego 30A, Poznań, PL'
      },
      {
        'latitude': 52.39719238,
        'longitude': 16.92828346
      },
      {
        'latitude': 52.41235609,
        'longitude': 16.92219015
      },
      {
        'latitude': 52.39204517,
        'longitude': 16.9031238
      },
      {
        'latitude': 52.39159395,
        'longitude': 16.90133364
      },
      {
        'latitude': 52.41405938,
        'longitude': 16.93109433
      }
    ]
  },
  {
    'nodeInfo': {
     'nodeAddress': {
       'latitude': 52.40258802,
       'longitude': 16.89606486
     }
    },
    'clients': [
      {
        'latitude': 52.41184018,
        'longitude': 16.90699465
      },
      {
        'latitude': 52.42281178,
        'longitude': 16.9026721
      },
      {
        'latitude': 52.39967806,
        'longitude': 16.93213413
      },
      {
        'latitude': 52.41607343,
        'longitude': 16.8900285
      },
      {
        'latitude': 52.42367186,
        'longitude': 16.89440013
      },
      {
        'latitude': 52.41186527,
        'longitude': 16.90204187
      },
      {
        'latitude': 52.41458117,
        'longitude': 16.93739438
      },
      {
        'latitude': 52.4199398,
        'longitude': 16.9356219
      },
      {
        'latitude': 52.41777522,
        'longitude': 16.908121
      },
      {
        'latitude': 52.42555707,
        'longitude': 16.92383228
      }
    ]
  }
]

var mapObj = new Mapa('map1')

axios({
    method: 'post',
    url: '/getmap',
    data: whatToFetch
  })
  .then((response) => mapObj.prepareMap(response.data))
  .catch((error) => console.log(error))