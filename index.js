const bodyParser = require('body-parser')
const NodeGeocoder = require('node-geocoder')
const async = require('async')
const cors = require('cors')
const express = require('express')
const app = express()
const port = 3000
const options = {
  provider: 'openstreetmap'
}
const geocoder = NodeGeocoder(options)
app.use(bodyParser.json())
app.use(cors())
app.use(express.static('dist'))

app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With')
  next()
})

/**
 *
 * Calculates degrees from radians
 *
 * @param {number} radians radians to convert
 */

function radiansToDegrees (radians) {
  var pi = Math.PI
  return radians * (180 / pi)
}

/**
 * Calculates radians from degrees
 *
 * @param {number} degrees degrees to convert
 */

function degreesToRadians (degrees) {
  var pi = Math.PI
  return degrees * (pi / 180)
}

/**
 * Calculate the bearing from the transceiver to the client.
 * @param {Object} transceiver Object representing the transceiver.
 * @param {number} transceiver.latitude Latitude of the transceiver.
 * @param {number} transceiver.longitude Longitude of the transceiver.
 * @param {Object} client Object representing a client.
 * @param {number} client.latitude Latitude of the client.
 * @param {number} client.longitude Longitude of the client.
 * @returns {number} Bearing from transceiver to client.
 */

function angleOf (transceiver, client) {
  let startLat = degreesToRadians(transceiver.latitude)
  let startLng = degreesToRadians(transceiver.longitude)
  let destLat = degreesToRadians(client.latitude)
  let destLng = degreesToRadians(client.longitude)

  let y = Math.sin(destLng - startLng) * Math.cos(destLat)
  let x = Math.cos(startLat) * Math.sin(destLat) - Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng)
  let brng = Math.atan2(y, x)
  brng = radiansToDegrees(brng)
  return (brng + 360) % 360
}

/**
 * Calculate the distance from the transceiver to the client.
 * @param {Object} transceiver Object representing the transceiver.
 * @param {number} transceiver.latitude Latitude of the transceiver.
 * @param {number} transceiver.longitude Longitude of the transceiver.
 * @param {Object} client Object representing a client.
 * @param {number} client.latitude Latitude of the client.
 * @param {number} client.longitude Longitude of the client.
 * @returns {number} Distance from transceiver to client.
 */

function distanceFrom (transceiver, client) {
  var R = 6371e3 // metres
  var latitude1 = degreesToRadians(transceiver.latitude)
  var latitude2 = degreesToRadians(client.latitude)
  var distancelatitude = degreesToRadians((client.latitude - transceiver.latitude))
  var distancelongitude = degreesToRadians((client.longitude - transceiver.longitude))

  var a = Math.sin(distancelatitude / 2) * Math.sin(distancelatitude / 2) +
    Math.cos(latitude1) * Math.cos(latitude2) *
    Math.sin(distancelongitude / 2) * Math.sin(distancelongitude / 2)
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

  var d = R * c
  return d
}

// API endpoint for getting the map info.
// TODO: maybe use try/catch?

app.post('/getmap', function (req, res) {
  let container = req.body // Container is the request body
  let returnedArray = [] // Array that will later be returned to the client
  // Geocoding
  async.each(container, (element, callback) => {
    let returningObject = {} // Object that will later be pushed to the returnedArray
    // Geocode the transceivers
    if (element.nodeInfo.nodeAddress.name == null) { // If you can't find an address, do a reverse geocode.
      console.log(element.nodeInfo.nodeAddress)
      geocoder.reverse({
        lat: element.nodeInfo.nodeAddress.latitude,
        lon: element.nodeInfo.nodeAddress.longitude
      })
        .then((res) => {
          console.log(res[0])
          returningObject.nodeInfo = {} // Information about the transceiver
          returningObject.clients = [] // Information about the clients
          returningObject.nodeInfo.nodeAddress = res[0] // Where is the transceiver?
          returnedArray.push(returningObject) // Store the transceiver information in the returnedArray
          async.each(element.clients, (client, callback) => {
            // Geocode the clients
            if (client.name == null) { // If you can't find an address, do a reverse geocode.
              geocoder.reverse({
                lat: client.latitude,
                lon: client.longitude
              })
                .then((res) => {
                  let returner = res[0]
                  returner.angle = angleOf({ // Calculate the bearing from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: client.latitude,
                    longitude: client.longitude
                  })
                  returner.distance = distanceFrom({ // Calculate the distance from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: client.latitude,
                    longitude: client.longitude
                  })
                  returningObject.clients.push(returner) // Store the information about the client in the clients array in the returningObject object.
                  callback()
                })
                .catch(err => console.log(err))
            } else { // If you can find an address, geocode it.
              geocoder.geocode(client.name)
                .then((res) => {
                  let returner = res[0]
                  returner.angle = angleOf({ // Calculate the bearing from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: returner.latitude,
                    longitude: returner.longitude
                  })
                  returner.distance = distanceFrom({ // Calculate the distance from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: returner.latitude,
                    longitude: returner.longitude
                  })
                  returningObject.clients.push(returner) // Store the information about the client in the clients array in the returningObject object.
                  callback()
                })
                .catch(err => console.log(err))
            }
          }, function (err) {
            console.log(err)
            callback()
          })
        })
        .catch(function (err) {
          callback(err)
        })
    } else { // If you can find an address, geocode it.
      geocoder.geocode(element.nodeInfo.nodeAddress.name)
        .then((res) => {
          returningObject.nodeInfo = {} // Information about the transceiver
          returningObject.clients = [] // Information about the clients
          returningObject.nodeInfo.nodeAddress = res[0] // Where is the transceiver?
          returnedArray.push(returningObject) // Store the transceiver information in the returnedArray
          async.each(element.clients, (client, callback) => {
            // Geocode the clients
            if (client.name == null) { // If you can't find an address, do a reverse geocode.
              geocoder.reverse({
                lat: client.latitude,
                lon: client.longitude
              })
                .then((res) => {
                  let returner = res[0]
                  returner.angle = angleOf({ // Calculate the bearing from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: client.latitude,
                    longitude: client.longitude
                  })
                  returner.distance = distanceFrom({ // Calculate the distance from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: client.latitude,
                    longitude: client.longitude
                  })
                  returningObject.clients.push(returner) // Store the information about the client in the clients array in the returningObject object.
                  callback()
                })
                .catch(err => console.log(err))
            } else { // If you can find an address, geocode it.
              geocoder.geocode(client.name)
                .then((res) => {
                  let returner = res[0]
                  returner.angle = angleOf({ // Calculate the bearing from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: returner.latitude,
                    longitude: returner.longitude
                  })
                  returner.distance = distanceFrom({ // Calculate the distance from the transceiver to the client
                    latitude: returningObject.nodeInfo.nodeAddress.latitude,
                    longitude: returningObject.nodeInfo.nodeAddress.longitude
                  }, {
                    latitude: returner.latitude,
                    longitude: returner.longitude
                  })
                  returningObject.clients.push(returner) // Store the information about the client in the clients array in the returningObject object.
                  callback()
                })
                .catch(err => console.log(err))
            }
          }, function (err) {
            console.log(err)
            callback()
          })
        })
        .catch((err) => callback(err))
    }
  }, function (err) {
    console.log(err)
    returnedArray.forEach((element) => {
      element.clients.sort(function (a, b) { // Sort by bearing
        return a.angle - b.angle
      })
    })
    res.end(JSON.stringify(returnedArray))
  })
})

app.listen(port, () => console.log(`Server is running on the ${port} port.`))
